set nocompatible              " be iMproved, required
set shell=/bin/bash
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The theme, first
Plugin 'buztard/vim-marshmallow'

Plugin 'scrooloose/nerdtree'
Plugin 'SirVer/ultisnips'
Plugin 'bling/vim-airline'
Plugin 'chase/vim-ansible-yaml'
Plugin 'dag/vim-fish'
Plugin 'tomtom/tcomment_vim'

Plugin 'vim-scripts/L9'
Plugin 'vim-scripts/FuzzyFinder'

Plugin 'rking/ag.vim'
""Plugin 'mekanix/Vim-Autoclose'
Plugin 'klen/python-mode'
Plugin 'ivanov/vim-ipython'
Plugin 'xolox/vim-easytags'
Plugin 'xolox/vim-misc'
Plugin 'majutsushi/tagbar'
Plugin 'vim-scripts/apachelogs.vim'
Plugin 'vim-scripts/vim-multiedit'
Plugin 'scrooloose/syntastic'

Plugin 'mattn/webapi-vim'
Plugin 'mattn/gist-vim'
Plugin 'burnettk/vim-angular'
Plugin 'pangloss/vim-javascript'
Plugin 'othree/javascript-libraries-syntax.vim'
Plugin 'emmet.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-eunuch'
Plugin 'vim-scripts/Gundo'

Plugin 'ervandew/supertab'

"" PlantUML plugins.
Plugin 'sava-j/vim-slumlord'
Plugin 'aklt/plantuml-syntax'

Plugin 'mhinz/vim-signify'

Plugin 'pechorin/any-jump.vim'

Plugin 'cpiger/NeoDebug'

" clang-format plugin
Plugin 'rhysd/vim-clang-format'

call vundle#end()

filetype plugin indent on     " Required!

map <C-n> :NERDTreeToggle<CR>

inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}

inoremap (      ()<Left>
inoremap (<CR>  (<CR>)<Esc>O
inoremap ((     (
inoremap ()     ()

inoremap [      []<Left>
inoremap [<CR>  [<CR>]<Esc>O
inoremap [[     [
inoremap []     []

inoremap '      ''<Left>
inoremap '<CR>  '<CR>'<Esc>O
inoremap ''     '
inoremap ''     ''

inoremap "      ""<Left>
inoremap "<CR>  "<CR>"<Esc>O
inoremap ""     "
inoremap ""     ""

inoremap <      <><Left>
inoremap <<CR>  <<CR>><Esc>O
inoremap <<     <
inoremap <<     <<

function! LinenumberToggle()
    set number
    if(&relativenumber == 1)
        set norelativenumber
    else
        set relativenumber
    endif
endfunc

map <C-m> :call LinenumberToggle()<CR>

set ignorecase
set smartcase

:autocmd InsertLeave * if &ft =~ 'plantuml' | silent call slumlord#updatePreview({'write': 1}) | endif

setlocal spell spelllang=en_us

" Auto-enable clang-format for C/C++ sources.
autocmd FileType cpp ClangFormatAutoEnable
autocmd FileType c ClangFormatAutoEnable

" Let clang-format run on leaving insert mode.
let g:clang_format#auto_formatexpr=1

" When .clang-format file is not found, don't run clang-format. For some
" reason this is necessary in order to use .clang-format file.
let g:clang_format#enable_fallback_style=0

" Automatically detect and use .clang-format file.
let g:clang_format#detect_style_file=1
