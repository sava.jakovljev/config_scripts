#!/bin/bash

sudo dnf install -y \
cmake \
gcc-c++ \
gcovr \
sbc-devel \
pulseaudio-libs-devel \
systemd-devel \
openssl-devel \
libuv-devel \
jansson-devel \
alsa-lib-devel \
gstreamer1-devel \
gstreamer1-plugins-base-tools \
gstreamer1-plugins-base-devel \
libsoup-devel \
libunwind-devel \
flac-devel \
libvorbis-devel \
libogg-devel \
protobuf-devel \
avahi-devel \
patch \
make \
libtool \
python3-devel \
socat \
the_silver_searcher

git submodule update --init --recursive

# Override original vimrc provided by Atlantic, so it would be used during
# vundle init phase.
cp -f ${PWD}/vimrc ${PWD}/vim/

# Remove old config, if any, and invoke init from Atlantic
rm -rf ~/.vim

bash vim/bin/init.sh
cp tmux.conf ~/.tmux.conf

# Override .vimrc explicitelly
cp -f vimrc ~/.vimrc

cat HashBang/Hashbang >> ~/.vimrc

# Fix some annoying errors
cp -f ${PWD}/marshmallow.vim ${PWD}/vim/vim/bundle/vim-marshmallow/colors

# "git lg" alias
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
